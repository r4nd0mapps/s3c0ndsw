/// <reference path="../../../typings/angularjs/angular-mocks.d.ts" />
/// <reference path="../../../typings/mocha/mocha.d.ts" />
/// <reference path="../../../typings/chai/chai.d.ts" />
/// <reference path="../../../typings/moment/moment.d.ts" />
/// <reference path="../../../typings/moment/moment-node.d.ts" />
/// <reference path="../../../app/scripts/filters/formatduration.ts" />

"use strict";

describe("Filter: formatDuration", () => {

  // load the filter"s module
  beforeEach(angular.mock.module("swApp"));

  // initialize a new instance of the filter before each test
  let formatDuration;
  beforeEach(inject($filter => {
    formatDuration = $filter("formatDuration");
  }));

  it("should return the input prefixed with 'formatDuration filter:'", () => {
    let text = moment.duration("0:1:10");
    formatDuration(text, "HH:mm:ss").should.equal("00:01:10");
  });

});
