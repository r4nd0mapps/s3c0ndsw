/// <reference path="../../../typings/angularjs/angular-mocks.d.ts" />
/// <reference path="../../../typings/mocha/mocha.d.ts" />
/// <reference path="../../../typings/chai/chai.d.ts" />
/// <reference path="../../../typings/chai-as-promised/chai-as-promised.d.ts" />
/// <reference path="../../../typings/es6-promise/es6-promise.d.ts" />
/// <reference path="../../../app/scripts/services/intervalliststoreservice.ts" />

"use strict";

describe("Service: IntervalListStoreService", () => {

  // load the service"s module
  beforeEach(angular.mock.module("swApp"));

  // // instantiate service
  // let iss;
  // beforeEach(inject(intervalStoreService => {
  //   iss = intervalStoreService;
  // }));

  it("should do something", (done) => {
    return Promise.resolve(2 + 2)
      .should.eventually.equal(4).and.notify(done);
  });
});

