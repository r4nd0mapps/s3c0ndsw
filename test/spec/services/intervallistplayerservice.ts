/// <reference path="../../../typings/angularjs/angular-mocks.d.ts" />
/// <reference path="../../../typings/mocha/mocha.d.ts" />
/// <reference path="../../../typings/chai/chai.d.ts" />
/// <reference path="../../../typings/chai-as-promised/chai-as-promised.d.ts" />
/// <reference path="../../../typings/es6-promise/es6-promise.d.ts" />
/// <reference path="../../../app/scripts/services/intervallistplayerservice.ts" />
/// <reference path="../../../typings/moment/moment.d.ts" />
/// <reference path="../../../typings/moment/moment-node.d.ts" />

"use strict";

describe("Service: IntervalListPlayerService", () => {

  beforeEach(angular.mock.module("swApp"));

  let runInfo = new swApp.IntervalListRunInfo();
  let player: swApp.IntervalListPlayer;
  beforeEach(inject(() => {
    player = new swApp.IntervalListPlayerService();
  }));

  let stubIntervalList1 = () => [
    new swApp.Interval("1 Int - Ex 1", moment.duration("0:0:03")),
  ];

  it("1 Interval - Verify properties at start", (done) => {
    player.initialize(runInfo, stubIntervalList1());

    Promise.resolve(riToString(runInfo))
      .should.eventually.deep.equal("00:00:03, 0, 1, 00:00:00, 00:00:03")
      .and.notify(done);
  });

  it("1 Interval - Verify properties at first play", (done) => {
    player.initialize(runInfo, stubIntervalList1());

    let ret = playNTimes(player, 1);

    ret
      .should.eventually.deep.equal("00:00:03, 1, 1, 00:00:00, 00:00:03")
      .and.notify(done);
  });

  it("1 Interval - Verify properties at middle play", (done) => {
    player.initialize(runInfo, stubIntervalList1());

    let ret = playNTimes(player, 2);

    ret
      .should.eventually.deep.equal("00:00:02, 1, 1, 00:00:01, 00:00:02")
      .and.notify(done);
  });

  it("1 Interval - Verify properties at last but 1 play", (done) => {
    player.initialize(runInfo, stubIntervalList1());

    let ret = playNTimes(player, 3);

    ret
      .should.eventually.deep.equal("00:00:01, 1, 1, 00:00:02, 00:00:01")
      .and.notify(done);
  });

  it("1 Interval - Verify properties at last play", (done) => {
    player.initialize(runInfo, stubIntervalList1());

    let ret = playNTimes(player, 4);

    ret
      .should.eventually.deep.equal("00:00:00, 1, 1, 00:00:03, 00:00:00")
      .and.notify(done);
  });

  let stubIntervalList2 = () => [
    new swApp.Interval("2 Int - Ex 1", moment.duration("0:0:3")),
    new swApp.Interval("2 Int - Ex 2", moment.duration("0:0:5")),
  ];

  it("2 Interval - Verify properties at transition play for 1", (done) => {
    player.initialize(runInfo, stubIntervalList2());

    let ret = playNTimes(player, 4);

    ret
      .should.eventually.deep.equal("00:00:05, 2, 2, 00:00:03, 00:00:05")
      .and.notify(done);
  });

  it("2 Interval - Verify properties at first play for 2", (done) => {
    player.initialize(runInfo, stubIntervalList2());

    let ret = playNTimes(player, 5);

    ret
      .should.eventually.deep.equal("00:00:04, 2, 2, 00:00:04, 00:00:04")
      .and.notify(done);
  });

  it("2 Interval - Verify properties at last play for 2", (done) => {
    player.initialize(runInfo, stubIntervalList2());

    let ret = playNTimes(player, 9);

    ret
      .should.eventually.deep.equal("00:00:00, 2, 2, 00:00:08, 00:00:00")
      .and.notify(done);
  });

  function playNTimes(p: swApp.IntervalListPlayer, n: number) {
    let ret: any;

    for (let i = 0; i < n; i++) {
      ret = p.play();
    }

    return ret.then(function(val) {
      return Promise.resolve(riToString(val));
    });
  }

  function durationToString(input: moment.Duration) {
    return moment.utc(input.asMilliseconds()).format("HH:mm:ss");
  }

  function riToString(ri: swApp.IntervalListRunInfo) {
    return durationToString(ri.currentRemainingTime)
      + ", " + ri.currentInterval
      + ", " + ri.totalIntervals
      + ", " + durationToString(ri.totalElapsedTime)
      + ", " + durationToString(ri.totalRemainingTime);
  }

});
