/// <reference path="../../../typings/angularjs/angular-mocks.d.ts" />
/// <reference path="../../../typings/mocha/mocha.d.ts" />
/// <reference path="../../../typings/chai/chai.d.ts" />
/// <reference path="../../../app/scripts/controllers/main.ts" />

"use strict";

describe("Controller: MainCtrl", () => {

  // load the controller's module
  beforeEach(angular.mock.module("swApp"));

  let mainCtrl: swApp.MainCtrl;
  let scope: swApp.IMainScope;

  // Initialize the controller and a mock scope
  beforeEach(angular.mock.inject(($controller: ng.IControllerService, $rootScope: ng.IRootScopeService) => {
    scope = <any>$rootScope.$new();
    mainCtrl = <any>$controller("MainCtrl", {
      $scope: scope
    });
  }));

  it("should start with 0 todo items", () => {
    scope.todos.length.should.equal(0);
  });

  it("should add items to the list", () => {
    scope.todo = "Item 1";
    mainCtrl.addTodo();
    scope.todos.should.deep.equal(["Item 1"]);
    scope.todo.should.equal("");
  });

  it("should add then remove an item from the list", () => {
    scope.todo = "Item 1";
    mainCtrl.addTodo();
    mainCtrl.removeTodo(0);
    scope.todos.should.deep.equal([ ]);
    scope.todo.should.equal("");
  });
});
