/// <reference path="../../../typings/angularjs/angular-mocks.d.ts" />
/// <reference path="../../../typings/mocha/mocha.d.ts" />
/// <reference path="../../../typings/chai/chai.d.ts" />
/// <reference path="../../../app/scripts/controllers/about.ts" />

"use strict";

describe("Controller: AboutCtrl", () => {

  // load the controller's module
  beforeEach(() => { angular.mock.module("swApp"); });

  let aboutCtrl: swApp.AboutCtrl;
  let scope: swApp.IAboutScope;

  // Initialize the controller and a mock scope
  beforeEach(angular.mock.inject(($controller: ng.IControllerService, $rootScope: ng.IRootScopeService) => {
    scope = <any>$rootScope.$new();
    aboutCtrl = <any>$controller("AboutCtrl", {
      $scope: scope
    });
  }));

  it("should start with 3 awesomeThings", () => {
    scope.awesomeThings.length.should.equal(3);
  });
});
