/// <reference path="../../../app/scripts/iids.ts" />
/// <reference path="../../../typings/angularjs/angular-mocks.d.ts" />
/// <reference path="../../../typings/mocha/mocha.d.ts" />
/// <reference path="../../../typings/chai/chai.d.ts" />
/// <reference path="../../../typings/chai-as-promised/chai-as-promised.d.ts" />
/// <reference path="../../../typings/es6-promise/es6-promise.d.ts" />
/// <reference path="../../../app/scripts/model/intervalliststore.ts" />
/// <reference path="../../../app/scripts/model/intervallistplayer.ts" />
/// <reference path="../../../app/scripts/controllers/playlist.ts" />
/// <reference path="../../../typings/moment/moment.d.ts" />
/// <reference path="../../../typings/moment/moment-node.d.ts" />

"use strict";

describe("Controller: PlayListController", () => {

  class StubIntervalStoreService implements swApp.IntervalListStore {
    public getInterval(name: string) {
      return [ ];
    }
  }

  class StubIntervalListPlayerService implements swApp.IntervalListPlayer {
    public initialize(ri: swApp.IntervalListRunInfo, is: swApp.Interval[]): void {
      return;
    }

    public play(): Promise<swApp.IntervalListRunInfo> {
      return null;
    }
  }

  let plc: swApp.PlayListController;
  let scope: swApp.IPlayListScope;
  let iss: swApp.IntervalListStore = new StubIntervalStoreService();
  let ilps: swApp.IntervalListPlayer = new StubIntervalListPlayerService();

  beforeEach(() => {
    angular.mock.module(
      "swApp",
      $provide => {
        $provide.value(swApp.IIDs.INTERVALLISTSTORESERVICE, iss);
        $provide.value(swApp.IIDs.INTERVALLISTPLAYERSERVICE, ilps);
      });
  });

  beforeEach(
    angular.mock.inject(
      ($controller: ng.IControllerService, $rootScope: ng.IRootScopeService) => {
        scope = <any>$rootScope.$new();
        plc = <any>$controller(swApp.IIDs.PLAYLISTCONTROLLER, { $scope: scope });
      }));

  it("should inject dependent services correctly", () => {
    plc.is.should.be.equal(iss);
    plc.ip.should.be.equal(ilps);
  });

});
