/// <reference path="./iids.ts" />
/// <reference path="../../typings/angularjs/angular.d.ts" />
/// <reference path="../../typings/angularjs/angular-route.d.ts" />
/// <reference path="../../typings/moment/moment.d.ts" />
/// <reference path="../../typings/moment/moment-node.d.ts" />

"use strict";

angular.module("swApp", [
    "ngAnimate",
    "ngCookies",
    "ngResource",
    "ngRoute",
    "ngSanitize",
    "ngTouch",
    "ui.sortable",
    "angularMoment"
  ])
  .config(["$routeProvider", ($routeProvider: ng.route.IRouteProvider) => {
    $routeProvider
      .when("/", {
        controller: "MainCtrl",
        templateUrl: "views/main.html"
      })
      .when("/about", {
        controller: "AboutCtrl",
        templateUrl: "views/about.html"
      })
      .when("/playlist/:ilid", {
        controller: swApp.IIDs.PLAYLISTCONTROLLER,
        templateUrl: "views/playlist.html"
      })
      .otherwise({
        redirectTo: "/"
      });
  }]);
