/// <reference path="../iids.ts" />
/// <reference path="../app.ts" />
/// <reference path="../model/interval.ts" />

"use strict";

module swApp {
  export interface IntervalListStore {
    getInterval(name: string): Interval[];
  }
}
