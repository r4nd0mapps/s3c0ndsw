/// <reference path="../iids.ts" />

"use strict";

module swApp {
  export interface Platform {
    setTimeOut(callback: () => void, timeOut: number): void;
  }
}
