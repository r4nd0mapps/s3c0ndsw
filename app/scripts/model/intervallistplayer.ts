/// <reference path="../iids.ts" />
/// <reference path="interval.ts" />
/// <reference path="../../../typings/es6-promise/es6-promise.d.ts" />

"use strict";

module swApp {
  export interface IntervalListPlayer {
    initialize(ri: IntervalListRunInfo, is: Interval[]): void;
    play(): Promise<IntervalListRunInfo>;
  }
}
