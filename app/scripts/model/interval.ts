/// <reference path="../iids.ts" />
/// <reference path="../../../typings/moment/moment.d.ts" />
/// <reference path="../../../typings/moment/moment-node.d.ts" />

"use strict";

module swApp {

  export class Interval {
    public status: string;
    public name: string;
    public duration: moment.Duration;

    constructor(name: string, duration: moment.Duration) {
      this.status = "UP NEXT";
      this.name = name;
      this.duration = duration;
    }
  }

  export class IntervalListRunInfo {
    public currentRemainingTime: moment.Duration;
    public currentInterval: number;
    public totalIntervals: number;
    public totalElapsedTime: moment.Duration;
    public totalRemainingTime: moment.Duration;
  }
}
