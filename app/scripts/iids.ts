"use strict";

module swApp {
  export class IIDs {
    public static get PLAYLISTCONTROLLER(): string { return "PlayListController"; }

    public static get INTERVALLISTSTORESERVICE(): string { return "IntervalListStoreService"; }
    public static get PLATFORMSERVICE(): string { return "PlatformService"; }
    public static get INTERVALLISTPLAYERSERVICE(): string { return "IntervalListPlayerService"; }
  }
}
