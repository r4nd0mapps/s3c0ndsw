/// <reference path="../app.ts" />

"use strict";

module swApp {
  export interface IAboutScope extends ng.IScope {
    awesomeThings: string[];
  }

  export class AboutCtrl {
    constructor (private $scope: IAboutScope) {
    this.$scope.awesomeThings = [
        "HTML5 Boilerplate",
        "AngularJS",
        "Karma",
      ];
    }
  }
}

angular.module("swApp")
  .controller("AboutCtrl", ["$scope", ($scope: swApp.IAboutScope) => new swApp.AboutCtrl($scope)]);
