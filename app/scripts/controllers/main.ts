/// <reference path="../app.ts" />

"use strict";

module swApp {
  export interface IMainScope extends ng.IScope {
    todos: string[];
    todo: string;
  }

  export class MainCtrl {
    constructor(private $scope: IMainScope) {
      this.$scope.todos = [];
    }

    public addTodo() {
      this.$scope.todos.push(this.$scope.todo);
      this.$scope.todo = "";
    }

    public removeTodo(index: number) {
      this.$scope.todos.splice(index, 1);
    }
  }
}

angular.module("swApp")
  .controller("MainCtrl", ["$scope", ($scope: swApp.IMainScope) => new swApp.MainCtrl($scope)]);
