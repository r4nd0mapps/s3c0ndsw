/// <reference path="../iids.ts" />
/// <reference path="../app.ts" />
/// <reference path="../../../typings/moment/moment.d.ts" />
/// <reference path="../../../typings/moment/moment-node.d.ts" />
/// <reference path="../../../typings/underscore/underscore.d.ts" />
/// <reference path="../../../typings/es6-promise/es6-promise.d.ts" />
/// <reference path="../model/interval.ts" />
/// <reference path="../model/intervalliststore.ts" />
/// <reference path="../model/intervallistplayer.ts" />
/// <reference path="../model/platform.ts" />

"use strict";

module swApp {

  export interface IPlayListScope extends ng.IScope {
    runInfo: IntervalListRunInfo;
    intervals: Interval[];
  }

  export class PlayListController {
    public static $inject = ["$scope", "$routeParams", IIDs.INTERVALLISTSTORESERVICE, IIDs.INTERVALLISTPLAYERSERVICE, IIDs.PLATFORMSERVICE];

    constructor(
      private $scope: IPlayListScope,
      private $routeParams,
      public is: IntervalListStore,
      public ip: IntervalListPlayer,
      public ps: Platform) {
      this.initialize();
    }

    /*
    TODO: P2D: In the wireup of controller, subscribe to the Promise and have a unit that does the following
    - Scroll to view
    - Registration with timer
    - Sounds
    - Call itself back in another second
     */

    public initialize() {
      this.$scope.intervals = this.is.getInterval(this.$routeParams.ilid);
      this.$scope.runInfo = new IntervalListRunInfo();

      this.$scope.runInfo.currentRemainingTime =
        this.$scope.intervals.length ? this.$scope.intervals[0].duration : moment.duration("0:0:0");
      this.$scope.runInfo.currentInterval = 0;
      this.$scope.runInfo.totalIntervals = this.$scope.intervals.length;
      this.$scope.runInfo.totalElapsedTime = moment.duration("0:0:0");
      this.$scope.runInfo.totalRemainingTime = this.$scope.intervals.reduce((acc, e) => acc.add(e.duration), moment.duration("0:0:0"));

      this.ip.initialize(this.$scope.runInfo, this.$scope.intervals);
    }

    public play(): void {
      this.ip.play();
    }
  }
}

angular.module("swApp").controller(swApp.IIDs.PLAYLISTCONTROLLER, swApp.PlayListController);
