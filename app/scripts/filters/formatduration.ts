/// <reference path="../app.ts" />
/// <reference path="../../../typings/moment/moment.d.ts" />
/// <reference path="../../../typings/moment/moment-node.d.ts" />

"use strict";

module swApp {
  export function formatDurationFilterFactory(): Function {
    return formatDurationFilter;
  }

  function formatDurationFilter(input: moment.Duration, format: string) {
    return moment.utc(input.asMilliseconds()).format(format);
  }
}

angular.module("swApp")
  .filter("formatDuration", swApp.formatDurationFilterFactory);
