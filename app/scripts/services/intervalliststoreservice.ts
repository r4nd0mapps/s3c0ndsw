/// <reference path="../iids.ts" />
/// <reference path="../app.ts" />
/// <reference path="../model/intervalliststore.ts" />

"use strict";

module swApp {
  export class IntervalListStoreService implements swApp.IntervalListStore {
    public getInterval(name: string): swApp.Interval[] {
      return {
        "7mwo": [
          new Interval("Exercise 1", moment.duration("0:0:20")),
          new Interval("Exercise 2", moment.duration("0:0:10")),
          new Interval("Exercise 3", moment.duration("0:0:01")),
          new Interval("Exercise 4", moment.duration("0:0:2")),
          new Interval("Exercise 5", moment.duration("0:0:2")),
        ],
      }[name];
    }
  }
}

angular.module("swApp").service(swApp.IIDs.INTERVALLISTSTORESERVICE, swApp.IntervalListStoreService);
