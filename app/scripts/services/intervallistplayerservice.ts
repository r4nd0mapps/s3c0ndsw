/// <reference path="../app.ts" />
/// <reference path="../model/interval.ts" />
/// <reference path="../model/intervallistplayer.ts" />
/// <reference path="../../../typings/es6-promise/es6-promise.d.ts" />

"use strict";

module swApp {
  export class IntervalListPlayerService implements swApp.IntervalListPlayer {
    private runInfo: IntervalListRunInfo;
    private intervals: Interval[];

    public initialize(ri: IntervalListRunInfo, is: Interval[]): void {
      this.runInfo = ri;
      this.intervals = is;
      this.runInfo.currentRemainingTime = is[0].duration;
      this.runInfo.currentInterval = 0;
      this.runInfo.totalIntervals = is.length;
      this.runInfo.totalElapsedTime = moment.duration("0:0:0");
      this.runInfo.totalRemainingTime = is.reduce((acc, e) => acc.add(e.duration), moment.duration("0:0:0"));
    }

    public play(): Promise<IntervalListRunInfo> {
      let isFirstPlay = ri => ri.currentInterval === 0;
      let isTransition = (ri, int) => ri.currentRemainingTime.asSeconds() === 0 && int.length > ri.currentInterval;

      if (isFirstPlay(this.runInfo)) {
        this.runInfo.currentInterval++;
      } else {
        let oneSec = moment.duration(Number(1), "seconds");
        this.runInfo.currentRemainingTime.subtract(oneSec);
        this.runInfo.totalElapsedTime.add(oneSec);
        this.runInfo.totalRemainingTime.subtract(oneSec);

        if (isTransition(this.runInfo, this.intervals)) {
          this.runInfo.currentRemainingTime = this.intervals[this.runInfo.currentInterval].duration;
          this.runInfo.currentInterval++;
        }
      }

      return Promise.resolve(this.runInfo);
    }
  }
}

angular.module("swApp").service(swApp.IIDs.INTERVALLISTPLAYERSERVICE, swApp.IntervalListPlayerService);
